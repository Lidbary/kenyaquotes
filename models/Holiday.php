<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "holidays".
 *
 * @property integer $id
 * @property string $data
 * @property integer $category
 * @property integer $is_deleted
 * @property integer $created_by
 * @property string $created_at
 * @property string $modified_at
 * @property string $description
 * @property string title
 * @property integer status
 * @property string $date
 */
class Holiday extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'holidays';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'data', 'category', 'is_deleted', 'created_by', 'created_at', 'modified_at', 'description', 'date'], 'required'],
            [['id', 'category', 'is_deleted', 'created_by','status'], 'integer'],
            [['data', 'description','title'], 'string'],
            [['created_at', 'modified_at', 'date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'data' => 'Data',
            'category' => 'Category',
            'is_deleted' => 'Is Deleted',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'modified_at' => 'Modified At',
            'description' => 'Description',
            'date' => 'Date',
        ];
    }
}
