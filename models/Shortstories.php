<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "short_stories".
 *
 * @property integer $id
 * @property string $data
 * @property integer $category
 * @property integer $is_deleted
 * @property integer $created_by
 * @property string $created_at
 * @property string $modified_at
 */
class Shortstories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'short_stories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['data', 'category', 'is_deleted', 'created_by', 'created_at', 'modified_at'], 'required'],
            [['data'], 'string'],
            [['category', 'is_deleted', 'created_by'], 'integer'],
            [['created_at', 'modified_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'data' => 'Data',
            'category' => 'Category',
            'is_deleted' => 'Is Deleted',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'modified_at' => 'Modified At',
        ];
    }
}
