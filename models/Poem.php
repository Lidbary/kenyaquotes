<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "poem".
 *
 * @property integer $id
 * @property string $poem
 * @property integer $category
 * @property integer $is_deleted
 * @property integer $created_by
 * @property string $created_at
 * @property string $modified_at
 */
class Poem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'poem';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['poem', 'category', 'is_deleted', 'created_by', 'created_at', 'modified_at'], 'required'],
            [['poem'], 'string'],
            [['category', 'is_deleted', 'created_by'], 'integer'],
            [['created_at', 'modified_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'poem' => 'Poem',
            'category' => 'Category',
            'is_deleted' => 'Is Deleted',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'modified_at' => 'Modified At',
        ];
    }
}
