<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Mchongwano */

$this->title = 'Create Mchongwano';
$this->params['breadcrumbs'][] = ['label' => 'Mchongwanos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mchongwano-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
