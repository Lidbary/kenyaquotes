<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'Kenya Quotes',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Home', 'url' => ['/site/index']],
            ['label' => 'Short Stories', 'url' => ['/shortstories/index']],
            ['label' => 'Mchongwano', 'url' => ['/mchongwano/index']],
            ['label' => 'Did You Know', 'url' => ['/didyouknow/index']],
            ['label' => 'Poems', 'url' => ['/poem/index']],
            ['label' => 'Holidays', 'url' => ['/holidays/holiday/index']],
            Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post', ['class' => 'navbar-form'])
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?php
        $sidecls = "col-sm-2";
        $maincls = "col-sm-10";
        if(Yii::$app->controller->module->id == 'holidays') {
            ?>
            <div class="<?= $sidecls ?>">
                <?php
//                echo SideNav::widget([
//                    'type' => SideNav::TYPE_DEFAULT,
//                    'heading' => 'Options',
//                    'items' => [
//                        ['label' => 'Customer', 'url' => ['#'],'items' => [
//                            ['label' => 'Customer', 'url' => ['/crud/customer/index','workerid'=>$workerid]],
//                            ['label' => 'Customer phone', 'url' => ['/crud/customerphone/index','workerid'=>$workerid]],
//                            ['label' => 'Customer map', 'url' => ['/crud/customer/map','workerid'=>$workerid]],
//                        ],'visible'=>$crudmenu],
//                            ['label' => 'Suplier', 'url' => ['#'],'items' =>[
//                        ['label' => 'Supplier', 'url' => ['/crud/supplier/index','workerid'=>$workerid]],
//                                ['label' => 'Supplier Log', 'url' => ['/crud/supplierlog/index','workerid'=>$workerid]],
//                            ],'visible'=>$crudmenuworker],
//                        ],
//                    ]);
                ?>
            </div>
            <div class="<?= $maincls?>">
                <?= $content ?>
            </div>
            <?php
        }else {
            ?>
            ?>
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= $content ?>
            <?php
        }
        ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
