<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Poem */

$this->title = 'Update Poem: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Poems', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="poem-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
