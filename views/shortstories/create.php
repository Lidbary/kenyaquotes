<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ShortStories */

$this->title = 'Create Short Stories';
$this->params['breadcrumbs'][] = ['label' => 'Short Stories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="short-stories-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
