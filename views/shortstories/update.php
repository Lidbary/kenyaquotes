<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ShortStories */

$this->title = 'Update Short Stories: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Short Stories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="short-stories-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
