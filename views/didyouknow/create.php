<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Didyouknow */

$this->title = 'Create Didyouknow';
$this->params['breadcrumbs'][] = ['label' => 'Didyouknows', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="didyouknow-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
